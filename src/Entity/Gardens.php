<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="gardens")
 * @ORM\Entity(repositoryClass="App\Repository\GardensRepository")
 */
class Gardens
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     * @Groups("api")
     * @ORM\OneToMany(targetEntity="Galleries", mappedBy="gardenId")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     * @Groups("api")
     */
    private $name;

    /**
    * @ORM\Column(type="string", length=50)
    * @Groups("api")
    */
    private $descShort;

    /**
    * @ORM\Column(type="string", length=1000)
    * @Groups("api")
    */
    private $descLong;

    /**
    * @ORM\Column(type="integer")
    * @Groups("api")
    */
    private $longitude;


     /**
    * @ORM\Column(type="integer")
    * @Groups("api")
    */
    private $latitude;

   

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this Gardens.
     *
     * @see GardensInterface
     */
    public function getName(): string
    {
        return (string) $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
    * @see GardensInterface
    */
    public function getDescShort()
    {
        return $this->descShort;
    }

    public function setDescShort($descShort)
    {
        $this->descShort = $descShort;
    }


    /**
    * @see GardensInterface
    */
    public function getDescLong()
    {
        return $this->descLong;
    }

    public function setDescLong($descLong)
    {
        $this->descLong = $descLong;
    }


    /**
    * @see GardensInterface
    */
    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }


    /**
    * @see GardensInterface
    */
    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }
}
