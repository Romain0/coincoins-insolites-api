<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="galleries")
 * @ORM\Entity(repositoryClass="App\Repository\GalleriesRepository")
 */
class Galleries 
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("api")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups("api")
     */
    private $uri;

    /**
    * @ORM\Column(type="string")
    * @Groups("api")
    */
    private $data;

    /**
    * @Groups("api")
    * @ORM\Column(name="garden_id", type="integer")
    * @ORM\ManyToOne(targetEntity="Gardens", inversedBy="id")
    * @ORM\JoinColumn(name="garden_id", referencedColumnName="id")
    */
    private $gardenId;

    
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this Galleries.
     *
     * @see GalleriesInterface
     */
    public function getUri(): string
    {
        return (string) $this->uri;
    }

    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    /**
    * @see GalleriesInterface
    */
    public function getData()
    {
        return $this->data;
    }

    public function setData(string $data)
    {
        $this->data = $data;
    }


    /**
    * @see GalleriesInterface
    */
    public function getGardenId()
    {
        return $this->gardenId;
    }

    public function setGardenId($gardenId)
    {
        $this->gardenId = $gardenId;
    }

}
