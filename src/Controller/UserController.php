<?php

namespace App\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class UserController extends AbstractController {
    /**
    * @Route("/api/register", name="api_register", methods={"POST"})
    */
    public function register(EntityManagerInterface $om, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator, Request $request){
    
        $data = json_decode($request->getContent(), true);

        $emailConstraint = new Assert\Email();
        // all constraint "options" can be set this way
        $emailConstraint->message = 'Invalid email address';
        
        $user = new User();
        $username               = $data['login'];
        $email                  = $data['email'];
        $password               = $data['password'];
        $passwordConfirmation   = $data['confirmPassword'];
        $errors = [];

        $checkEmail = $validator->validate(
            $email,
            $emailConstraint
        );

        if (0 != count($checkEmail)) {
            $errors[] = $checkEmail[0]->getMessage();
        }
        
        if($password != $passwordConfirmation){
            $errors[] = "Password does not match the password confirmation.";
        }

        if($username === null || strlen($username) < 5){
            $errors[] = "Your username is less than 5 characters.";
        }

        if($email === null || strlen($email) < 5){
            $errors[] = "Your email is less than 5 characters.";
        }

        if(strlen($password) < 6){
            $errors[] = "Password should be at least 6 characters.";
        }

        if(!$errors){
            $encodedPassword = $passwordEncoder->encodePassword($user, $password);
            $user->setEmail($email);
            $user->setUsername($username);
            $user->setPassword($encodedPassword);
            $om->persist($user);
            $om->flush();
            return $this->json([
                'user' => $user
            ]);
        }
  
        return $this->json([
            'errors' => $errors
        ], 400);
    }

    /**
    * @Route("/api/login", name="api_login", methods={"POST"})
    */
    public function login(){
        return $this->json(['result' => true]);
    }

    /**
    * @Route("/api/profile", name="api_profile")
    * @IsGranted("ROLE_USER")
    */
    public function profile(){
        return $this->json([
                'user' => $this->getUser()
            ], 200, [], 
            [
                'groups' => ['api']
            ]
        );
    }
}
