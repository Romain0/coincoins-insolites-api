<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Contact;

class ContactController extends AbstractController {
    /**
     * @Route("/api/contact", name="contact", methods={"POST"})
     */
    public function index(EntityManagerInterface $om, ValidatorInterface $validator, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $emailConstraint = new Assert\Email();
        $emailConstraint->message = 'Invalid email address';

        $contact = new Contact();

        $name               = $data['name'];
        $email              = $data['email'];
        $subject            = $data['subject'];
        $content            = $data['content'];

        $errors = [];

        $checkEmail = $validator->validate(
            $email,
            $emailConstraint
        );

        if (0 != count($checkEmail)) {
            $errors[] = $checkEmail[0]->getMessage();
        }

        if($name === null || strlen($name) < 3){
            $errors[] = "Your name is less than 3 characters.";
        }

        if($email === null || strlen($email) < 5){
            $errors[] = "Your email is less than 5 characters.";
        }

        if($subject === null || strlen($subject) < 10){
            $errors[] = "Your subject is less than 10 characters.";
        }

        if(strlen($content) < 25){
            $errors[] = "Your message is less than 25 characters.";
        }

        if(!$errors){
            $contact->setName($name);
            $contact->setEmail($email);
            $contact->setSubject($subject);
            $contact->setContent($content);
            $om->persist($contact);
            $om->flush();
            return $this->json([
                'result' => true,
                'contact' => $contact
            ]);
        }
  
        return $this->json([
            'result' => false,
            'errors' => $errors
        ], 400);
    }
}
