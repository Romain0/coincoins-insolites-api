<?php

namespace App\Controller;



use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Gardens;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class GardensController extends AbstractController {
    /**
     * @Route("/api/gardens", name="gardens", methods={"GET"})
     */
    public function index(EntityManagerInterface $om, Request $request)
    {

        $repository = $om->getRepository(Gardens::class);
        $gardens = $repository->findAll();
        
        return $this->json([
            $gardens
        ], 200);
    }

}