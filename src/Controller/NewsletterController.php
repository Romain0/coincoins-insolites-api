<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Newsletter;

class NewsletterController extends AbstractController {
    /**
     * @Route("/api/newsletter", name="newsletter", methods={"POST"})
     */
    public function index(EntityManagerInterface $om, ValidatorInterface $validator, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $emailConstraint = new Assert\Email();
        $emailConstraint->message = 'Invalid email address';

        $newsletter = new Newsletter();
        $email              = $data['email'];
        $errors = [];

        $checkEmail = $validator->validate(
            $email,
            $emailConstraint
        );

        if (0 != count($checkEmail)) {
            $errors[] = $checkEmail[0]->getMessage();
        }

        if($email === null || strlen($email) < 5){
            $errors[] = "Your email is less than 5 characters.";
        }

        if(!$errors){
            $newsletter->setEmail($email);
            $om->persist($newsletter);
            $om->flush();
            return $this->json([
                'result' => true,
                'newsletter' => $newsletter
            ]);
        }
  
        return $this->json([
            'result' => false,
            'newsletter' => $errors
        ], 400);
    }
}
