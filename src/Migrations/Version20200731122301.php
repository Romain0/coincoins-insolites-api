<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200731122301 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE galleries DROP FOREIGN KEY FK_F70E6EB7424AC512');
        $this->addSql('DROP INDEX IDX_F70E6EB7424AC512 ON galleries');
        $this->addSql('ALTER TABLE galleries CHANGE garden_id_id garden_id INT NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE galleries CHANGE garden_id garden_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE galleries ADD CONSTRAINT FK_F70E6EB7424AC512 FOREIGN KEY (garden_id_id) REFERENCES gardens (id)');
        $this->addSql('CREATE INDEX IDX_F70E6EB7424AC512 ON galleries (garden_id_id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
