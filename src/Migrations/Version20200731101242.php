<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200731101242 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE galleries ADD CONSTRAINT FK_F70E6EB739F3B087 FOREIGN KEY (garden_id) REFERENCES gardens (id)');
        $this->addSql('CREATE INDEX IDX_F70E6EB739F3B087 ON galleries (garden_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE galleries DROP FOREIGN KEY FK_F70E6EB739F3B087');
        $this->addSql('DROP INDEX IDX_F70E6EB739F3B087 ON galleries');
    }
}
