<?php

namespace App\Repository;

use App\Entity\Gardens;
use App\Entity\Galleries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Gardens|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gardens|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gardens[]    findAll()
 * @method Gardens[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GardensRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gardens::class);
    }

    /**
    * @return Gardens[]
    */
    public function findAll(){
        $em = $this->getEntityManager();
        return $em->createQuery('SELECT g, ga FROM App\Entity\Gardens g JOIN App\Entity\Galleries ga WHERE g.id = ga.gardenId')->getResult();
    }

    // /**
    //  * @return Gardens[] Returns an array of Garden objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Garden
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
