<?php

namespace App\Repository;

use App\Entity\Galleries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;


class GalleriesRepository extends ServiceEntityRepository 
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Galleries::class);
    }

    public function findAll(){
        $query = $this->getDoctrine()
        ->getRepository('CoreBundle:Galleries')
        ->createQueryBuilder('c')
        ->getQuery();
      return $query->getResult(Query::HYDRATE_ARRAY);
    }
}
